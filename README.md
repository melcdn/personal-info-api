# Personal Info API

## Method and Endpoint Details
---
<details>
<summary>List Users</summary>

Path: `/users`

Method: `GET`

`[Response 200]`:
```json
"Users": [
    {
        "id": 1,
        "name": "Mel",
        "address": "Philippines",
        "birthdate": "2021-09-23",
        "mobile_number": "09255442118"
    }
]
```

</details>


<details>
<summary>List Users by Name</summary>

Path: `/users`

Query Parameters: 
* `name`: string


Method: `GET`


`[Response 200]`:
```json
"Users": [
    {
        "id": 1,
        "name": "Mel",
        "address": "Philippines",
        "birthdate": "2021-09-23",
        "mobile_number": "09255442118"
    }
]
```
</details>

<details>
<summary>Get User by Id</summary>

Path: `/users/{id}`

Method: `GET`

`Response 200`:
```json
{
    "id": 1,
    "name": "Mel",
    "address": "Philippines",
    "birthdate": "2021-09-23",
    "mobile_number": "09255442118"
}
```
`Response 404`:
```json
{
    "message": "user not found."
}
```

</details>

<details>
<summary>Create New User</summary>

Path: `/users`

Method: `POST`

Body: 
```json
{
    "name": "Mel",
    "address": "Philippines",
    "birthdate": "2021-09-23",
    "mobile_number": "09255442118"
}

```

`Response 200`:
```json
{
    "User": { 
        "id": 2,   
        "name": "Mel",
        "address": "Philippines",
        "birthdate": "2021-09-23",
        "mobile_number": "09255442118"
    },
    "message": "user successfully created."
}
```

</details>


<details>
<summary>Update User Details</summary>

Path: `/users/{id}`

Method: `PUT`

Body: 
```json
{
    "name": "Mel",
    "address": "Philippines",
    "birthdate": "2021-09-23",
    "mobile_number": "09255442118"
}

```

`Response 200`:
```json
{
    "message": "user details updated successfully."
}
```
`Response 404`:
```json
{
    "message": "user not found."
}
```
</details>

<details>
<summary>Delete User</summary>

Path: `/users/{id}`

Method: `DELETE`

`Response 200`:
```json
{
    "message": "user deleted successfully."
}
```
`Response 404`:
```json
{
    "message": "user not found."
}
```
</details>






