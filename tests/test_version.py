import unittest
from src.api import lambda_fns


class TestVersion(unittest.TestCase):
    def setUp(self) -> None:
        self.version = "0.1.0"

    def tearDown(self) -> None:
        pass

    def test_version_number(self):
        self.assertEquals(lambda_fns.__version__, self.version)